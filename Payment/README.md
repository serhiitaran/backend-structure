# Payment

Микросервис который будет заниматься оплатой и хранить всю информацию в своей локальной базе данных. Также в этом микросервисе будут находиться свои сервисы:

1. кредиты платформы;
2. промокоды;
3. долг;
4. депозиты;
5. cпособы оплаты; 
6. генерация инвойсов.